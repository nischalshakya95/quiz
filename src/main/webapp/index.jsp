<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Interview Question</title>
</head>
<link rel="stylesheet" href="./assets/css/style.css">
<body>
<h1 align = "center">Multiple Choice Question For Interview </h1>
<div class="panel">
    <form action="quiz" method ="post">
        <div class="container">
            <h4>Q.1 Is Java a platform independent language?</h4>
            <label>a)</label>
            <input type="radio" id="oa" name="q1" value="a">
            Yes
            <br>
            <label >b)</label>
            <input type="radio" id="ob" name="q1" value="b">
            No

            <h4>Q.2 Is Java not a pure object oriented language?</h4>
            <label>a) </label>
            <input type="radio" id="twa" name="q2" value="a">
            Yes<br>
            <label >b)</label>
            <input type="radio" id="twb" name="q2" value="b">
            No

            <h4>Q.3 Why does Java not make use of pointers?</h4>
            <label>a) </label>
            <input type="radio" id="tha" name="q3" value="a">
             Because it is unsafe<br>
            <label >b)</label>
            <input type="radio" id="thb" name="q3" value="b">
             Because it is too easy

            <h4>Q.4 What do you mean by data encapsulation?</h4>
            <label>a) </label>
            <input type="radio" id="foa" name="q4" value="a">
            Concept of hiding the data attributes and their behaviors in a single unit <br>
            <label >b)</label>
            <input type="radio" id="fob" name="q4" value="b">
            Process of creating multiple constructors in the class consisting of the same name

            <h4>Q.5 What is the main objective of garbage collection?</h4>
            <label>a) </label>
            <input type="radio" id="fia" name="q5" value="a">
            Ensures that the memory resource is used efficiently<br>
            <label >b)</label>
            <input type="radio" id="fib" name="q5" value="b">
            Detecting multiple constructor

            <h4>Q.6 Can the static methods be overridden?</h4>
            <label>a) </label>
            <input type="radio" id="sia" name="q6" value="a">
            Yes<br>
            <label >b)</label>
            <input type="radio" id="sib" name="q6" value="b">
            No

            <h4>Q.7 What part of memory - Stack or Heap - is cleaned in garbage collection process?</h4>
            <label>a) </label>
            <input type="radio" id="sea" name="q7" value="a">
            Stack<br>
            <label >b)</label>
            <input type="radio" id="seb" name="q7" value="b">
            Heap

            <h4>Q.8 Main purpose of JDK.</h4>
            <label>a) </label>
            <input type="radio" id="eia" name="q8" value="a">
            For environment creation to execute the code<br>
            <label >b)</label>
            <input type="radio" id="eib" name="q8" value="b">
            For code development and execution

            <h4>Q.9 Why we use constructor?</h4>
            <label>a)</label>
            <input type="radio" id="nia" name="q9" value="a">
             For exposing the object's behavior<br>
            <label >b)</label>
            <input type="radio" id="nib" name="q9" value="b">
            For initializing the object state

            <h4>Q.10 Java works as “pass by value” or “pass by reference” phenomenon?</h4>
            <label>a)</label>
            <input type="radio" id="tea" name="q10" value="a">
             Pass by value<br>
            <label >b)</label>
            <input type="radio" id="teb" name="q10" value="b">
            Pass by reference
        </div>
        <button type="submit" class="btn">Submit Answer</button>
    </form>
</div>
</body>
</html>