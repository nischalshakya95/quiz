package com.midterm.mcq;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "quiz", value = "/quiz")
public class Quiz extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int count=0;

        String q1 = request.getParameter("q1");
        String q2 = request.getParameter("q2");
        String q3 = request.getParameter("q3");
        String q4 = request.getParameter("q4");
        String q5 = request.getParameter("q5");
        String q6 = request.getParameter("q6");
        String q7 = request.getParameter("q7");
        String q8 = request.getParameter("q8");
        String q9 = request.getParameter("q9");
        String q10 = request.getParameter("q10");

        if(q1.equals("a")){
            out.println("(q1=true, ");
           count++;
        }else{
            out.println("(q1=false, ");
        }
        if(q2.equals("a")){
            out.println("q2=true, ");
            count++;
        }else{
            out.println("q2=false, ");
        }
        if(q3.equals("a")){
            out.println("q3=true, ");
            count++;
        }else{
            out.println("q3=false, ");
        }
        if(q4.equals("a")){
            out.println("q4=true, ");
            count++;
        }else{
            out.println("q4=false, ");
        }
        if(q5.equals("a")){
            out.println("q5=true, ");
            count++;
        }else{
            out.println("q5=false, ");
        }
        if(q6.equals("b")){
            out.println("q6=true, ");
            count++;
        }else{
            out.println("q6=false, ");
        }
        if(q7.equals("b")){
            out.println("q7=true, ");
            count++;
        }else{
            out.println("q7=false, ");
        }
        if(q8.equals("b")){
            out.println("q8=true, ");
            count++;
        }else{
            out.println("q8=false, ");
        }
        if(q9.equals("b")){
            out.println("q9=true, ");
            count++;
        }else{
            out.println("q9=false, ");
        }
        if(q10.equals("b")){
            out.println("q10=true)");
            count++;
        }else{
            out.println("q10=false)");
        }

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("answer.html");
            out.println("your total point = " + count);
        requestDispatcher.include(request, response);


    }
}
